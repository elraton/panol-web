import CartNumber from './cart-number-plugin.js';

document.addEventListener('DOMContentLoaded', function() {
    const cartNumber = new CartNumber({
        container: document.querySelector('#number-badge'),
        initCounter: 0
    });

    const buttonsAddCart = document.querySelectorAll('.button-add-cart');
    for (const button of buttonsAddCart) {
        button.onclick = (e) => {
            e.preventDefault();
            e.stopPropagation();
            const modalAddCart = M.Modal.getInstance(document.querySelector('#modal-add-cart'));
            if (modalAddCart) {
                cartNumber.addNumber(1);
                modalAddCart.open();
            }
        };
    }

    M.Dropdown.init(document.querySelectorAll('.dropdown-trigger'), {
        alignment: 'left',
        coverTrigger: false
    });
    for (const obj of document.querySelectorAll('.custom-dropdown')) {
        for (const a of obj.querySelectorAll('a')) {
            a.innerHTML = '<span class="badge material-icons">keyboard_arrow_right</span>' + a.innerHTML;
        }
    }

    if (document.querySelector('.glide')) {
        var glide = new Glide('.glide', {
            type: 'carousel',
            startAt: 0,
            perView: 1,
            gap: 0
        });
    
        glide.mount();
    }

    if (document.querySelector('.custom-slider')) {
        var custom_slider = tns({
            container: '.custom-slider',
            items: 4,
            slideBy: 'page',
            autoplay: false,
            nav: false,
            controlsText: ['<img src="images/arrow_left_gray.svg">', '<img src="images/arrow_right_gray.svg">'],
            responsive: {
                300: {
                    items: 1
                },
                640: {
                    items: 2
                },
                1100: {
                    items: 3
                },
                1200: {
                    items: 4
                },
            }
        });
    }

    var menus = M.Sidenav.init(document.querySelectorAll('.sidenav'), {});
    var collapsibles = M.Collapsible.init(document.querySelectorAll('.collapsible'), {});

    M.FormSelect.init(document.querySelectorAll('.custom-select'));

    const selects = document.querySelectorAll('.select-wrapper')
    for (const obj of selects) {
        obj.onclick = () => {
            for (const obj of selects) {
                obj.classList.remove('active');
            }
            obj.classList.add('active');
        };
    }

    if (document.querySelector('.similar-slider-custom')) {
        var similar_slider = tns({
            container: '.similar-slider-custom',
            items: 4,
            slideBy: 'page',
            autoplay: false,
            nav: false,
            edgePadding: 25,
            controlsText: ['<img class="arrow_left" src="images/arrow_left_white.svg">', '<img class="arrow_right" src="images/arrow_right_white.svg">'],
            responsive: {
                300: {
                    items: 1
                },
                640: {
                    items: 2
                },
                1100: {
                    items: 3
                },
                1200: {
                    items: 4
                },
            }
        });
    }

    if (document.querySelectorAll('.modal')) {
        M.Modal.init(document.querySelectorAll('.modal'), {});
    }

});