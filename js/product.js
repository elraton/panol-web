document.addEventListener('DOMContentLoaded', function() {
    if (document.getElementById('price-slider')) {
        var slider = document.getElementById('price-slider');
        noUiSlider.create(slider, {
            start: [180, 3000.00],
            connect: true,
            step: .1,
            orientation: 'horizontal', // 'horizontal' or 'vertical'
            range: {
                'min': 180,
                'max': 3000
            },
            format: wNumb({
                decimals: 2
            })
        });

        document.querySelector('#minvalue').value = `S/${slider.noUiSlider.get()[0]}`;
        document.querySelector('#maxvalue').value = `S/${slider.noUiSlider.get()[1]}`;
        slider.noUiSlider.on('slide', function () {
            var slider = document.getElementById('price-slider');
            document.querySelector('#minvalue').value = `S/${slider.noUiSlider.get()[0]}`;
            document.querySelector('#maxvalue').value = `S/${slider.noUiSlider.get()[1]}`;
        });
    }

    if (document.querySelectorAll('.modal')) {
        M.Modal.init(document.querySelectorAll('.modal'), {});
    }

});