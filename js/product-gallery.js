document.addEventListener('DOMContentLoaded', function() {});

const galleryItems = document.querySelectorAll('.custom-gallery-product-container .image-list .item');
const container = document.querySelector('.custom-gallery-product-container .product-show');

galleryItems[0].classList.add('active');

const clearActive = () => {
    for (const obj of galleryItems) {
        obj.classList.remove('active');
    }    
};

const addImgOnContainer = (imgUrl) => {
    if (container.querySelector('img')) {
        container.querySelector('img').setAttribute("src", imgUrl);
    } else {
        container.innerHTML = `<img class="zoomImage" src="${imgUrl}" alt="zoom">`;
    }
};

const onClickItem = (item) => {
    clearActive();
    item.classList.add('active');
    addImgOnContainer(item.getAttribute('href'));
};

addImgOnContainer(galleryItems[0].getAttribute('href'));

for (const obj of galleryItems) {
    obj.onclick = (e) => {
        e.preventDefault();
        onClickItem(obj);
    };
    obj.onmouseenter = (e) => {
        e.preventDefault();
        onClickItem(obj);
    };
}

