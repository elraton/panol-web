import Compare from './compare-plugin.js';
const compareContainer = document.querySelector('.compare-items-container');
const compare = new Compare({container: compareContainer});

const compareButtons = document.querySelectorAll('.compare');
for (const obj of compareButtons) {
    const icon = document.createElement("img");
    icon.setAttribute('src', '/images/compare-active-icon.svg');
    icon.setAttribute('class', 'icon-compare');
    obj.appendChild(icon);
    obj.onclick = (e) => {
        e.stopPropagation();
        e.preventDefault();
        obj.classList.remove('active');
        obj.classList.add('active');
        const product = {
            id: obj.parentElement.parentElement.getAttribute('data-id'),
            title: obj.parentElement.querySelector('.name').innerHTML,
            subtitle: obj.parentElement.querySelector('.brand').innerHTML,
            image: obj.parentElement.parentElement.querySelector('.image-container img').getAttribute('src')
        }
        compare.addProduct(product);
        compare.render();
    };
}

const cleanButton = document.querySelector('.compare-container .clean');
if (cleanButton) {
    cleanButton.onclick = (e) => {
        e.preventDefault();
        compare.deleteAllProducts();
        for (const obj of compareButtons) {
            obj.classList.remove('active');
        }
    };
}

const compareButton = document.querySelector('.compare-button-red');
if (compareButton) {
    compareButton.onclick = (e) => {
        e.preventDefault();
        window.open(`/product-compare.html?products=${compare.getProducts()}`, '_blank');
    };
}