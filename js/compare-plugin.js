function Compare(config) {
    this.container = config.container;
    this.productList = [];
    this.initPlugin();
}

// product example 
/*
{
    id: number,
    title: string,
    subtitle: string,
    image: string,
}
*/

Compare.prototype.initPlugin = function () {
    this.container.innerHTML = '';
};

Compare.prototype.getProducts = function() {
    const ids = [];
    for (const obj of this.productList) {
        ids.push(obj.id);
    }
    return ids.join(',');
};

Compare.prototype.deleteAllProducts = function() {
    this.productList = [];
    this.render();
};

Compare.prototype.addProduct = function(product) {
    const productFinded = this.productList.find(x => x.id === product.id);
    if (!productFinded) {
        if (this.productList.length < 4) {
            this.productList.push(product);
        }
    }
};

Compare.prototype.deleteProduct = function(id) {
    this.productList = this.productList.filter(x => x.id !== id);
    console.log(this.container.querySelectorAll('div'));
    for (const obj of document.querySelectorAll('.product-list-container div')) {
        if (obj.getAttribute('data-id') === id) {
            obj.querySelector('.compare').classList.remove('active');
        }
    }
}

Compare.prototype.detectDeleteButtons = function() {
    const buttons = document.querySelectorAll('.delete-compare');
    for (const obj of buttons) {
        obj.onclick = (e) => {
            e.preventDefault();
            const id = obj.parentElement.getAttribute('data-id');
            this.deleteProduct(id);
            this.render();
        };
    }
}

Compare.prototype.render = function () {
    let html = '';
    for (const obj of this.productList) {
        html += `<div class="compare-item" data-id="${obj.id}">
            <div class="delete-compare">
                <img src="/images/delete-icon.svg">
            </div>
            <div class="image">
                <img src="${obj.image}">
            </div>
            <div class="text">
                <div class="title">${obj.title}</div>
                <div class="subtitle">${obj.subtitle}</div>
            </div>
        </div>`;
    }
    if (this.productList.length > 0) {
        if (!this.container.parentElement.parentElement.classList.contains('show')) {
            this.container.parentElement.parentElement.classList.add('show');
        }
    } else {
        this.container.parentElement.parentElement.classList.remove('show');
    }
    this.container.innerHTML = html;
    this.detectDeleteButtons();
};

export default Compare;