function CartNumber(config) {
    this.container = config.container;
    this.cartNumber = config.initCounter || 0;
}

CartNumber.prototype.initPlugin = function () {
    this.container.innerHTML = this.cartNumber;
};

CartNumber.prototype.addNumber = function(quantity) {
    this.cartNumber = this.cartNumber + Number(quantity);
    this.render();
}

CartNumber.prototype.substractNumber = function(quantity) {
    this.cartNumber = this.cartNumber - Number(quantity);
    this.render();
}

CartNumber.prototype.render = function () {
    this.container.innerHTML = this.cartNumber;
};

export default CartNumber;